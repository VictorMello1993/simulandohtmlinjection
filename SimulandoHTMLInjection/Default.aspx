﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SimulandoHTMLInjection.Default" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Simulando HTML Injection</title>
    <style>
        .lblCodificado, .lblDecodificado, .lblTextoTestado {
            position: relative;
            display: block;
            margin-top: 20px;
        }

        .titulo {
            text-align: center;
            font-size: 20pt;
        }
    </style>
</head>
<body>
    <header id="tituloPagina" class="titulo">SIMULANDO HTML INJECTION</header>
    <form id="form1" runat="server">
        <div id="ctnEntradaDados" runat="server">
            <asp:ScriptManager runat="server" ID="ScriptManager1" EnablePageMethods="true" />
            <script type="text/javascript">
                function verificarTextBox(txbInserir) {
                    if (txbInserir == '' || (!txbInserir.value.includes('<') && !txbInserir.value.includes('>') && !txbInserir.value.includes(' '))) {
                        alert('É preciso informar o código HTML!');
                    } else {
                        codificarEDecodificarHTML();
                    }
                }
                function codificarEDecodificarHTML() {
                    var htmlToEncode = document.getElementById(txbInserir.id).value;
                    var encodedHTML = escape(htmlToEncode);
                    var decodedHTML = unescape(encodedHTML);

                    var arrayStringHTML = decodedHTML.split('').map(ch => ch.replace('>', '') && ch.replace('<', '') && ch.replace('/', '')
                                          && ch.replace(' ', ''));

                    var StringHTMLDecodificado = arrayStringHTML.join('');

                    txaCodificado.innerText = txbInserir.value = encodedHTML;
                    txaDecodificado.innerText = StringHTMLDecodificado;
                }
            </script>
            <div id="ctnLabelInserirCodigoHTML" runat="server">
                <asp:Label ID="lblInserir" runat="server" />
                <asp:TextBox ID="txbInserir" runat="server" />
                <asp:Button ID="btnLimpar" runat="server" />
            </div>
            <div id="ctnTextoCodificado" runat="server">
                <asp:Label ID="lblCodificado" runat="server" CssClass="lblCodificado" />
                <textarea id="txaCodificado" runat="server" class="txaCodificado" style="height: 150px; width: 250px;" readonly="readonly" />
            </div>
            <div id="ctnTextoDecodificado" runat="server">
                <asp:Label ID="lblDecodificado" runat="server" CssClass="lblDecodificado" />
                <textarea id="txaDecodificado" runat="server" class="txaDecodificado" style="height: 150px; width: 250px;" readonly="readonly" />
            </div>
        </div>
    </form>
</body>
</html>