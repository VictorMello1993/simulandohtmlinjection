﻿using SimulandoHTMLInjection; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SimulandoHTMLInjection
{
    public partial class Default : System.Web.UI.Page

    {
      
        protected void Page_Load(object sender, EventArgs e)
        {
            SetFocus(txbInserir.ID);
            ConfigurarCampos();
        }

        private void ConfigurarCampos()
        {
            ConfigurarlblInserir();
            ConfigurartxbInserir();
            ConfigurarlblCodificado();
            ConfigurarlblDecodificado();
            ConfigurarbtnLimpar();
        }

        private void ConfigurarbtnLimpar()
        {
            btnLimpar.Text = TelaResource.Limpar;
            btnLimpar.Click += LimparCampos;
        }

        private void LimparCampos(object sender, EventArgs e)
        {
            txbInserir.Text = String.Empty;
            txaCodificado.InnerText = String.Empty;
            txaDecodificado.InnerText = String.Empty;
        }

        private void ConfigurarlblDecodificado()
        {
            lblDecodificado.Text = TelaResource.TextoDecodificado;
        }

        private void ConfigurarlblCodificado()
        {
            lblCodificado.Text = TelaResource.TextoCodificado;
        }

        private void ConfigurartxbInserir()
        {
            txbInserir.Attributes.Add("onchange", "verificarTextBox(this)");
        }

        private void ConfigurarlblInserir()
        {
            lblInserir.Text = TelaResource.EntreCodigoHTML;
        }
    }
}