# SimulandoHTMLInjection

Aplicação web nativo em ASP.NET, sem uso de framework de terceiros, que trata erros que possam ocasionar ataques de HTML Injection. Este conceito foi apreendido ao verificar essa vulnerabilidade em um dos sistemas ERP da empresa na qual trabalho.